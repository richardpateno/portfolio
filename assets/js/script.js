function scrollToTop() {
  if (window.scrollY > 0) {
    window.scrollTo(0, 0);
  }
}

window.addEventListener("animationstart", scrollToTop);

$(document).ready(function () {
  const animateCSS = (element, animation, animationEnd, prefix = "animate__") =>
    // We create a Promise and return it
    new Promise((resolve, reject) => {
      window.scrollTo(0, 0);
      const animationName = `${prefix}${animation}`;
      const animationEndName = `${prefix}${animationEnd}`;
      const node = document.querySelector(element);

      node.style.setProperty("--animate-duration", ".8s");
      node.classList.add(`${prefix}animated`, animationName);

      // When the animation ends, we clean the classes and resolve the Promise
      function handleAnimationEnd() {
        node.classList.remove(`${prefix}animated`, animationName);
        node.style.setProperty("animation-delay", "3s");
        node.classList.add(`${prefix}animated`, animationEndName);
        setTimeout(() => {
          document.querySelector(".myName").style.display = "none";
          document.querySelector(".fullView").style.display = "none";
          document.querySelector("body").classList.remove("stop-scrolling");
          $("body").unbind("touchmove");
          $(window).scrollTop(0);
          window.removeEventListener("animationstart", scrollToTop);
        }, 3800);
        resolve("Animation ended");
      }

      node.addEventListener("animationend", handleAnimationEnd, { once: true });
    });

  animateCSS(".myName", "fadeInUp", "fadeOutDown");

  $(document).bind("scroll", function (ev) {
    var scrollOffset = $(document).scrollTop();
    var containerOffset = $(".aboutMeImg").offset().top - window.innerHeight;
    if (scrollOffset > containerOffset) {
      $(".aboutMeCol").addClass([
        `animate__animated`,
        `animate__fadeIn`,
        `animate__slow`,
      ]);
    }

    var projectContainerOffset =
      $(".projectsTitle").offset().top - window.innerHeight;
    if (scrollOffset > projectContainerOffset) {
      $(".projectsTitle").addClass([
        `animate__animated`,
        `animate__fadeIn`,
        `animate__slow`,
      ]);
    }

    var aotContainerOffset = $(".aotRow").offset().top - window.innerHeight;
    if (scrollOffset > aotContainerOffset) {
      $(".aotRow").addClass([
        `animate__animated`,
        `animate__fadeIn`,
        `animate__slow`,
      ]);
    }

    var budgeterContainerOffset =
      $(".budgeterRow").offset().top - window.innerHeight;
    if (scrollOffset > budgeterContainerOffset) {
      $(".budgeterRow").addClass([
        `animate__animated`,
        `animate__fadeIn`,
        `animate__slow`,
      ]);
    }

    var tavernContainerOffset =
      $(".tavernTalesRow").offset().top - window.innerHeight;
    if (scrollOffset > tavernContainerOffset) {
      $(".tavernTalesRow").addClass([
        `animate__animated`,
        `animate__fadeIn`,
        `animate__slow`,
      ]);
    }

    var getInTouchContainerOffset =
      $(".getInTouch").offset().top - window.innerHeight;
    if (scrollOffset > getInTouchContainerOffset) {
      $(".getInTouch").addClass([
        `animate__animated`,
        `animate__fadeIn`,
        `animate__slow`,
      ]);
    }
  });
});

document.querySelector(".aboutNav").addEventListener("click", () => {
  document
    .querySelector(".aboutMeContainer")
    .scrollIntoView({ behavior: "smooth", block: "end" });
});

document.querySelector(".projectsNav").addEventListener("click", () => {
  document
    .querySelector(".allProjectsContainer")
    .scrollIntoView({ behavior: "smooth", block: "start" });
});

document.querySelector(".contactNav").addEventListener("click", () => {
  document
    .querySelector(".contactRow")
    .scrollIntoView({ behavior: "smooth", block: "start" });
});
